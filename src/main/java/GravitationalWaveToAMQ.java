import com.ie.sysmessage.TaskData;
import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.sf.json.JSONObject;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;


import javax.jms.*;

public class GravitationalWaveToAMQ {
	private static Log log = LogFactory.getLog(GravitationalWaveToAMQ.class);

	static final String NATIVE_LIBRARIES_32BIT = "/lib/native_libraries/32bits/";
	static final String NATIVE_LIBRARIES_64BIT = "/lib/native_libraries/64bits/";
	static final String NATIVE_LIBRARIES_64BIT_Linux = "/lib/64-bit-Linux/";
	static final String NATIVE_LIBRARIES_32BIT_Linux = "/lib/";
	static SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
	public static void main(String args[]){

		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
				new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class)
						.configure(params.xml()
								.setFileName("config.properties")
								.setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
		PropertiesConfiguration config=null;
		try {
			config= builder.getConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}


		//todo  test
		String planSrcPath = config.getString("planSrcPath" , "D:/test");
//		String planSrcPath = config.getString("planSrcPath" , "~/");
		final String amqUrl = config.getString("amqUrl" , "failover://(tcp://msg.xtelescope.net:61616)?randomize=false");
		final String gravitationalWaveTopic = config.getString("gravitationalWaveTopic" , "TaskTopic");
		int timeAfterCreate = config.getInt("timeAfterCreate" , 24*60*60*1000);
//		final String cameraWebFileFullPath = config.getString("cameraWebFileFullPath" , "D:/ftpRoot/cameraWebRoot/camera_1.jpg");
//		log.info(" 监视文件 " + cameraSrcPath);
		log.info(" 监视文件 " + planSrcPath);

		Scheduler sched = null;
		try {
			sched = new StdSchedulerFactory().getScheduler();
			sched.start();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("AutoDeleteFile_Trigger", "AutoDeleteFile_TriggerGroup")
//					.startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(60*60*24)
					.startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(30)
							.repeatForever()).build();
//			JobDetail initJob = JobBuilder.newJob(AutoDeleteJob.class).storeDurably()
//					.withIdentity("AutoDeleteFile", "AutoDeleteFile").build();
//			initJob.getJobDataMap().put("DirectoryRoot",cameraSrcPath);
//			initJob.getJobDataMap().put("TimeAfterCreate",timeAfterCreate);
//			sched.scheduleJob(initJob, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
			log.error(e);
		}

		Properties sysProps = System.getProperties();
		String osArch = (String) sysProps.get("os.arch");
		String osName = (String) sysProps.get("os.name");
		String userDir = (String) sysProps.getProperty("user.dir");
		log.info("os.arch: " + osArch);
		log.info("os.name: " + osName);
		log.info("userDir: " + userDir);
		log.info("java.class.path: " + sysProps.get("java.class.path"));

		// 直接调用Jnotify时， 会发生异常：java.lang.UnsatisfiedLinkError: no jnotify_64bit in java.library.path
		// 这是由于Jnotify使用JNI技术来加载dll文件，如果在类路径下没有发现相应的文件，就会抛出此异常。
		// 因此可以通过指定程序的启动参数: java -Djava.library.path=/path/to/dll，
		// 或者是通过修改JVM运行时的系统变量的方式来指定dll文件的路径，如下：

		// 判断系统是32bit还是64bit，决定调用对应的dll文件
		String jnotifyDir = "--";
		if(osName.toLowerCase().contains("linux")){
			jnotifyDir=NATIVE_LIBRARIES_32BIT_Linux;
			if (osArch.contains("64")) {
				jnotifyDir = NATIVE_LIBRARIES_64BIT_Linux;
			}
		}
		if(osName.toLowerCase().contains("windows")){
			jnotifyDir = NATIVE_LIBRARIES_32BIT;
			if (osArch.contains("64")) {
				jnotifyDir = NATIVE_LIBRARIES_64BIT;
			}
		}
		log.info("jnotifyDir: " + jnotifyDir);
		// 获取目录路径
		String pathToAdd = userDir + jnotifyDir ;
		boolean isAdded = false;
		Field usrPathsField = null;
		try {
			usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			log.error(e);
		}
		usrPathsField.setAccessible(true);
		String[] paths = new String[0];
		try {
			paths = (String[]) usrPathsField.get(null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			log.error(e);
		}
		log.info("usr_paths: " + Arrays.toString(paths));
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				isAdded  = true;
				break;
			}
		}
		if (!isAdded) {
			final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
			newPaths[newPaths.length - 1] = pathToAdd;
			try {
				usrPathsField.set(null, newPaths);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		log.info("java.library.path: " + System.getProperty("java.library.path"));
		try {
			log.info("usr_paths: " + Arrays.toString((String[]) usrPathsField.get(null)));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			log.error(e);
		}
		usrPathsField.setAccessible(false);
		log.info("类路径加载完成");

		// 监听F盘下的文件事件
		try {
			JNotify.addWatch( planSrcPath, JNotify.FILE_ANY, true, new JNotifyListener() {
                @Override
                public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
                    log.info("wd = " + wd + ", rootPath = " + rootPath);
                    log.info("oldName = " + oldName + ", newName = " + newName);
                }
                @Override
                public void fileModified(int wd, String rootPath, String fileName) {
//                    log.info("fileModified" + "          " + rootPath + "           " + fileName);
                }
                @Override
                public void fileDeleted(int wd, String rootPath, String fileName) {
//                    log.info("fileDeleted" + "          " + rootPath + "           " + fileName);
                }
                @Override
                public void fileCreated(int wd, String rootPath, String fileName) {
                    log.info("fileCreate" + "          " + rootPath + "           " + fileName);
					Path sourcePath = Paths.get(rootPath, fileName);
					String modifiedPath = String.valueOf(sourcePath);
					File modifiedFile = new File(modifiedPath);
					if(modifiedFile.isFile() && modifiedFile.exists() ){
						try {
							log.info("file =   "+ modifiedFile.getAbsolutePath());
							try {
								Thread.sleep(1000L);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
//							Files.copy(sourcePath , Paths.get(cameraWebFileFullPath) , REPLACE_EXISTING);
							String amqMsg = FileUtils.readFileToString(modifiedFile,"UTF-8");
							log.info(amqMsg);
							TaskData gwTask = new TaskData();
							gwTask.setTask_level("999");
							gwTask.setTaskName("GW_"+sdf.format(new Date())+fileName);
							gwTask.setTask_Ra_deg(0);
							gwTask.setTask_Dec_deg(0);
							gwTask.setTarget_eqp("GW_EQP");
							gwTask.setTask_type("GRAVITATIONALWAVE");
//							gwTask.setTask_type("GRAVITATIONALWAVE_Test");
							gwTask.setTask_plan_text(amqMsg);
							JSONObject jsonObject=JSONObject.fromObject(gwTask);
							sendAMQ(amqUrl, gravitationalWaveTopic, jsonObject.toString());
						} catch (IOException e) {
							e.printStackTrace();
							log.info("--",e);
						}
					}
                }
            });
		} catch (JNotifyException e) {
			e.printStackTrace();
		}
		while (true) {

		}

	}


	public static void sendAMQ(String amqURL, String gravitationalWaveTopic, String jsonMessage){
		ConnectionFactory connectionFactory;
		Connection connection = null;
		Session session;
		Destination destination;
		MessageProducer producer;
		TextMessage message = null;
		connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
				, ActiveMQConnection.DEFAULT_PASSWORD, amqURL);
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
			destination = session.createTopic(gravitationalWaveTopic);
			producer = session.createProducer(destination);
			//todo use json text message
			message=session.createTextMessage(jsonMessage);
			producer.send(message);
			session.commit();
		} catch (JMSException e) {
			log.error(e);
			e.printStackTrace();
		}finally {
			try {
				if (null != connection){
					connection.close();
				}
			} catch (Throwable ignore) {
			}
		}
	}


}

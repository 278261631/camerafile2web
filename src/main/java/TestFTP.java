import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.*;
import java.net.SocketException;

public class TestFTP {

   public static void main(String args[]) throws IOException {
       InputStream in = null ;
       FTPClient ftp=new FTPClient();
       String ftp_ip="localhost";
       int ftp_port=21;
       String subFolderName="simulator";
       ftp.connect(ftp_ip, ftp_port);
       String ftp_name="mayong";
       String ftp_pass="mayong";
       String filePath="D:\\test\\guidePreviewFull.png";
       String newFileName="guidePreviewFull.png";
       ftp.login(ftp_name, ftp_pass);
       String ftpSbuPath=subFolderName;
       ftp.enterLocalPassiveMode();
       String ftpFileName =filePath;
       String destFileName ="";
       String ftpDestFileName=newFileName;
       in = new BufferedInputStream(new FileInputStream(ftpFileName));

       boolean isSubDirectoryExsit = false;
       FTPFile[] dirs = ftp.listDirectories();
       if (dirs != null && dirs.length > 0) {
           for (int i = 0; i < dirs.length; i++) {
               if (dirs[i].getName().equals(ftpSbuPath)) {
                   isSubDirectoryExsit = true;
               }
               break;
           }
       }
       dirs = null;
       if (!isSubDirectoryExsit && !ftpSbuPath.equals("")) {
           ftp.makeDirectory(ftpSbuPath);
           destFileName = ftpSbuPath + "/" + ftpDestFileName;
       }
       if (isSubDirectoryExsit && !ftpSbuPath.equals("")) {
           destFileName = ftpSbuPath + "/" + ftpDestFileName;
       }

       ftp.enterLocalPassiveMode();
       ftp.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);
       ftp.setFileType(FTPClient.BINARY_FILE_TYPE);


       String LOCAL_CHARSET = "UTF-8";
       // FTP协议里面，规定文件名编码为iso-8859-1
       String SERVER_CHARSET = "ISO-8859-1";
       if (FTPReply.isPositiveCompletion(ftp.sendCommand("OPTS UTF8", "ON"))) {// 开启服务器对UTF-8的支持，如果服务器支持就用UTF-8编码，否则就使用本地编码（GBK）.
           LOCAL_CHARSET = "UTF-8";
       }
       ftp.setControlEncoding(LOCAL_CHARSET);
       destFileName = new String(destFileName.getBytes(LOCAL_CHARSET),SERVER_CHARSET);
       if (!ftp.storeFile(destFileName, in)){
           throw new IOException("Can't upload file '" + ftpFileName + "' to FTP server. Check FTP permissions and path." +ftp_ip+":"+ftp_port+":"+ftp_name);
       }
   }



}

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.TaskData;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.*;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * This is just a simple job that says "Hello" to the world.
 * </p>
 * 
 * @author Bill Kratzer
 */
public class DelFileJob implements Job {

    private static Logger _log = LoggerFactory.getLogger(DelFileJob.class);


    private static Log log = LogFactory.getLog(DelFileJob.class);
    static SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public DelFileJob() {
    }


        @Override
        public void execute(JobExecutionContext context)
            throws JobExecutionException {
            JobDataMap data = context.getJobDetail().getJobDataMap();
            String  fileName               = data.getString("fileName" );
            File fileToDel = new File(fileName);
            log.info("xxxxxxxxxxxxxxxxxx   " + fileName);
            fileToDel.delete();
        }
    }






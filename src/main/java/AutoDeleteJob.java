import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;

import java.io.File;
import java.io.FilenameFilter;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class AutoDeleteJob implements Job {

    private static Log log = LogFactory.getLog(AutoDeleteJob.class);
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("开始执行删除文件");
        JobDataMap data = context.getJobDetail().getJobDataMap();
        String directoryRootPath = data.get("DirectoryRoot").toString();
        Long timeAfterCreate = Long.parseLong(data.get("TimeAfterCreate").toString());

        File rootFile = new File(directoryRootPath);
        deleteSubFiles(rootFile , timeAfterCreate , System.currentTimeMillis());
        log.info("===" + timeAfterCreate + "===      "  + System.currentTimeMillis());
    }

    public void  deleteSubFiles(File rootDirectory , Long timeAfterCreate ,Long nowTime){
        for(File fileItem : rootDirectory.listFiles()){
            log.info(fileItem.getAbsolutePath());
            if(fileItem.isFile()){
                if(nowTime - fileItem.lastModified() > timeAfterCreate ){
                    log.info("Delete   Time = " +(nowTime - fileItem.lastModified())/1000+"S         "  + fileItem.getAbsolutePath() );
                    fileItem.delete();
                }
            }
            if (fileItem.isDirectory()){
                deleteSubFiles(new File(fileItem.getAbsolutePath()) , timeAfterCreate ,nowTime);
            }

        }
    }
}

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;

public class CameraFileToWeb{
	private static Log log = LogFactory.getLog(CameraFileToWeb.class);

	public static void main(String args[]){

		String cameraSrcPath = "D:\\ftpRoot\\cameraFileRoot";
		String cameraWebFileFullPath = "D:\\ftpRoot\\cameraWebRoot\\camera_1.jpg";
		log.info(" 监视文件 " + cameraSrcPath);
//		DeviceStatus deviceStatus = new DeviceStatus("WebCamera" , device_name ,"更新摄像头影像" , sdf.format(new Date()),device_color , MessageColor.OK);

		File fileRoot = new File(cameraSrcPath);
		if (!fileRoot.exists()){
			log.error("dir not exists   目录不存在  "+cameraSrcPath );
			fileRoot.mkdirs();
		}
		if (fileRoot.isFile()){
			log.error(" statusImageRoot 应该是路径而不是文件  "+cameraSrcPath );
		}

		try (WatchService ws = FileSystems.getDefault().newWatchService()) {
			Path dirToWatch = Paths.get(cameraSrcPath);
			dirToWatch.register(ws, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
			while (true) {
				WatchKey key = ws.take();
				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> eventKind = event.kind();
					if (eventKind == OVERFLOW) {
						System.out.println("Event  overflow occurred");
						continue;
					}
					if (eventKind == ENTRY_CREATE) {
						System.out.println("------------------     Copy To New image  -----------------------");
						WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
						Path dirEntry = currEvent.context();
						System.out.println(eventKind + "  occurred on  " + dirEntry);
//						new File(new Path())
						System.out.println("------------------  End Copy to New image  -----------------------");
						continue;
					}
					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
					Path dirEntry = currEvent.context();
					System.out.println(eventKind + "  occurred on  " + dirEntry);
				}
				boolean isKeyValid = key.reset();
				if (!isKeyValid) {
					System.out.println("No  longer  watching " + dirToWatch);
					break;
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}





	}
}

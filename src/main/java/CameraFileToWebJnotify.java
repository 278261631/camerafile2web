import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class CameraFileToWebJnotify {
	private static Log log = LogFactory.getLog(CameraFileToWebJnotify.class);

	static final String NATIVE_LIBRARIES_32BIT = "/lib/native_libraries/32bits/";
	static final String NATIVE_LIBRARIES_64BIT = "/lib/native_libraries/64bits/";
	public static void main(String args[]){

		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
				new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class)
						.configure(params.xml()
								.setFileName("config.properties")
								.setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
		PropertiesConfiguration config=null;
		try {
			config= builder.getConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
		}


		String cameraSrcPath = config.getString("cameraSrcPath" , "D:/ftpRoot/cameraFileRoot");
		int timeAfterCreate = config.getInt("timeAfterCreate" , 24*60*60*1000);
		final String cameraWebFileFullPath = config.getString("cameraWebFileFullPath" , "D:/ftpRoot/cameraWebRoot/camera_1.jpg");
		log.info(" 监视文件 " + cameraSrcPath);

		Scheduler sched = null;
		try {
			sched = new StdSchedulerFactory().getScheduler();
			sched.start();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("AutoDeleteFile_Trigger", "AutoDeleteFile_TriggerGroup")
					.withSchedule(CronScheduleBuilder.cronSchedule("0 0 12 * * ?")).build();
			JobDetail initJob = JobBuilder.newJob(AutoDeleteJob.class).storeDurably()
					.withIdentity("AutoDeleteFile", "AutoDeleteFile").build();
			initJob.getJobDataMap().put("DirectoryRoot",cameraSrcPath);
			initJob.getJobDataMap().put("TimeAfterCreate",timeAfterCreate);
			sched.scheduleJob(initJob, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

		Properties sysProps = System.getProperties();
		String osArch = (String) sysProps.get("os.arch");
		String osName = (String) sysProps.get("os.name");
		String userDir = (String) sysProps.getProperty("user.dir");
		log.info("os.arch: " + osArch);
		log.info("os.name: " + osName);
		log.info("userDir: " + userDir);
		log.info("java.class.path: " + sysProps.get("java.class.path"));

		// 直接调用Jnotify时， 会发生异常：java.lang.UnsatisfiedLinkError: no jnotify_64bit in java.library.path
		// 这是由于Jnotify使用JNI技术来加载dll文件，如果在类路径下没有发现相应的文件，就会抛出此异常。
		// 因此可以通过指定程序的启动参数: java -Djava.library.path=/path/to/dll，
		// 或者是通过修改JVM运行时的系统变量的方式来指定dll文件的路径，如下：

		// 判断系统是32bit还是64bit，决定调用对应的dll文件
		String jnotifyDir = NATIVE_LIBRARIES_64BIT;
		if (!osArch.contains("64")) {
			jnotifyDir = NATIVE_LIBRARIES_32BIT;
		}
		log.info("jnotifyDir: " + jnotifyDir);
		// 获取目录路径
		String pathToAdd = userDir + jnotifyDir ;
		boolean isAdded = false;
		Field usrPathsField = null;
		try {
			usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		usrPathsField.setAccessible(true);
		String[] paths = new String[0];
		try {
			paths = (String[]) usrPathsField.get(null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		log.info("usr_paths: " + Arrays.toString(paths));
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				isAdded  = true;
				break;
			}
		}
		if (!isAdded) {
			final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
			newPaths[newPaths.length - 1] = pathToAdd;
			try {
				usrPathsField.set(null, newPaths);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		log.info("java.library.path: " + System.getProperty("java.library.path"));
		try {
			log.info("usr_paths: " + Arrays.toString((String[]) usrPathsField.get(null)));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		usrPathsField.setAccessible(false);
		log.info("类路径加载完成");

		// 监听F盘下的文件事件
		try {
			JNotify.addWatch( cameraSrcPath, JNotify.FILE_ANY, true, new JNotifyListener() {
                @Override
                public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
                    log.info("wd = " + wd + ", rootPath = " + rootPath);
                    log.info("oldName = " + oldName + ", newName = " + newName);
                }
                @Override
                public void fileModified(int wd, String rootPath, String fileName) {
//                    log.info("fileModified" + "          " + rootPath + "           " + fileName);
                }
                @Override
                public void fileDeleted(int wd, String rootPath, String fileName) {
//                    log.info("fileDeleted" + "          " + rootPath + "           " + fileName);
                }
                @Override
                public void fileCreated(int wd, String rootPath, String fileName) {
                    log.info("fileCreate" + "          " + rootPath + "           " + fileName);
					Path sourcePath = Paths.get(rootPath, fileName);
					String modifiedPath = String.valueOf(sourcePath);
					File modifiedFile = new File(modifiedPath);
					if(modifiedFile.isFile() && modifiedFile.exists() ){
						try {
							log.info("copy file to web "+ modifiedFile.getAbsolutePath());
							try {
								Thread.sleep(2000L);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							Files.copy(sourcePath , Paths.get(cameraWebFileFullPath) , REPLACE_EXISTING);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
                }
            });
		} catch (JNotifyException e) {
			e.printStackTrace();
		}
		while (true) {

		}

	}

}

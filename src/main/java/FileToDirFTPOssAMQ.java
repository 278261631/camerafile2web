import com.ie.sysmessage.TaskData;
import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPReply;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.jms.*;
import java.io.*;
import java.lang.reflect.Field;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.quartz.impl.matchers.GroupMatcher;

public class FileToDirFTPOssAMQ {
	private static Log log = LogFactory.getLog(FileToDirFTPOssAMQ.class);

	static final String NATIVE_LIBRARIES_32BIT = "/lib/native_libraries/32bits/";
	static final String NATIVE_LIBRARIES_64BIT = "/lib/native_libraries/64bits/";
	static final String NATIVE_LIBRARIES_64BIT_Linux = "/lib/64-bit-Linux/";
	static final String NATIVE_LIBRARIES_32BIT_Linux = "/lib/";
	static SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
	public static void main(String args[]) throws SchedulerException {

		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
				new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class)
						.configure(params.xml()
								.setFileName("config.properties")
								.setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
		PropertiesConfiguration config=null;
		Scheduler scheduler = null;
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
			config= builder.getConfiguration();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}


//		String cameraSrcPath = config.getString("cameraSrcPath" , "D:/ftpRoot/cameraFileRoot");
		String fileSrcPath = config.getString("fileSrcPath" , "~/");
		final String amqUrl = config.getString("amqUrl" , "failover://(tcp://msg.xtelescope.net:61616)?randomize=false");
		final String amqTopic = config.getString("amqTopic" , "equipment_status");
//		final String gravitationalWaveTopic = config.getString("gravitationalWaveTopic" , "TaskTopic");
		int timeAfterCreate = config.getInt("timeAfterCreate" , 24*60*60*1000);
//		final String cameraWebFileFullPath = config.getString("cameraWebFileFullPath" , "D:/ftpRoot/cameraWebRoot/camera_1.jpg");
//		log.info(" 监视文件 " + cameraSrcPath);

		final boolean toFTP = config.getBoolean("toFTP" , false);
		final boolean toAMQ = config.getBoolean("toAMQ" , false);
		final boolean toOSS = config.getBoolean("toOSS" , false);
		final boolean doFileCreate = config.getBoolean("doFileCreate" , true);
		final boolean doFileModify = config.getBoolean("doFileModify" , false);
		final boolean javaInLinux = config.getBoolean("javaInLinux" , false);
		final boolean java32 = config.getBoolean("java32" , false);
		final String ftp_ip = config.getString("ftp_ip" , "localhost");
		final int ftp_port = config.getInt("ftp_port" , 21);
		final String ftp_name = config.getString("ftp_name" , "mayong");
		final String ftp_pass = config.getString("ftp_pass" , "mayong");
		final String device_name = config.getString("device_name" , "test");
		final String fileRegularExpression = config.getString("fileRegularExpression" , ".*");

		final String deviceName = config.getString("deviceName", "default");
		final String cameraImageUrl = config.getString("cameraImageUrl", "http://gyz.xtelescope.net:18084");
		final boolean ftpToSameFile = config.getBoolean("ftpToSameFile" , false);
		final String cameraImageFileName = config.getString("cameraImageFileName", "camera.png");
		final boolean delAfterFtp = config.getBoolean("delAfterFtp" , false);


		log.info(" 监视文件 " + fileSrcPath);

		final Scheduler sched = new StdSchedulerFactory().getScheduler();;

//			sched = new StdSchedulerFactory().getScheduler();
			sched.start();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("AutoDeleteFile_Trigger", "AutoDeleteFile_TriggerGroup")
//					.startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(60*60*24)
					.startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(30)
							.repeatForever()).build();
//			JobDetail initJob = JobBuilder.newJob(AutoDeleteJob.class).storeDurably()
//					.withIdentity("AutoDeleteFile", "AutoDeleteFile").build();
//			initJob.getJobDataMap().put("DirectoryRoot",cameraSrcPath);
//			initJob.getJobDataMap().put("TimeAfterCreate",timeAfterCreate);
//			sched.scheduleJob(initJob, trigger);


		Properties sysProps = System.getProperties();
		String osArch = (String) sysProps.get("os.arch");
		String osName = (String) sysProps.get("os.name");
		String userDir = (String) sysProps.getProperty("user.dir");
		log.info("os.arch: " + osArch);
		log.info("Is java 32: " + java32);
		log.info("os.name: " + osName);
		log.info("javaInLinux: " + javaInLinux);
		log.info("userDir: " + userDir);
		log.info("java.class.path: " + sysProps.get("java.class.path"));

		// 直接调用Jnotify时， 会发生异常：java.lang.UnsatisfiedLinkError: no jnotify_64bit in java.library.path
		// 这是由于Jnotify使用JNI技术来加载dll文件，如果在类路径下没有发现相应的文件，就会抛出此异常。
		// 因此可以通过指定程序的启动参数: java -Djava.library.path=/path/to/dll，
		// 或者是通过修改JVM运行时的系统变量的方式来指定dll文件的路径，如下：

		// 判断系统是32bit还是64bit，决定调用对应的dll文件
		String jnotifyDir = "--";
//		if(osName.toLowerCase().contains("linux")){
		if(javaInLinux){
			jnotifyDir=NATIVE_LIBRARIES_32BIT_Linux;
//			if (osArch.contains("64")) {
			if (!java32) {
				jnotifyDir = NATIVE_LIBRARIES_64BIT_Linux;
			}
		}
//		if(osName.toLowerCase().contains("windows")){
		if(!javaInLinux){
			jnotifyDir = NATIVE_LIBRARIES_32BIT;
//			if (!osArch.contains("64")) {
			if (!java32) {
				jnotifyDir = NATIVE_LIBRARIES_64BIT;
			}
		}
		log.info("jnotifyDir: " + jnotifyDir);
		// 获取目录路径
		String pathToAdd = userDir + jnotifyDir ;
		boolean isAdded = false;
		Field usrPathsField = null;
		try {
			usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			log.error(e);
		}
		usrPathsField.setAccessible(true);
		String[] paths = new String[0];
		try {
			paths = (String[]) usrPathsField.get(null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			log.error(e);
		}
		log.info("usr_paths: " + Arrays.toString(paths));
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				isAdded  = true;
				break;
			}
		}
		if (!isAdded) {
			final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
			newPaths[newPaths.length - 1] = pathToAdd;
			try {
				usrPathsField.set(null, newPaths);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		log.info("java.library.path: " + System.getProperty("java.library.path"));
		try {
			log.info("usr_paths: " + Arrays.toString((String[]) usrPathsField.get(null)));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			log.error(e);
		}
		usrPathsField.setAccessible(false);
		log.info("类路径加载完成");

		// 监听F盘下的文件事件
		try {
			final Scheduler finalScheduler = scheduler;
			JNotify.addWatch( fileSrcPath, JNotify.FILE_ANY, true, new JNotifyListener() {
                @Override
                public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
                    log.info("wd = " + wd + ", rootPath = " + rootPath);
                    log.info("oldName = " + oldName + ", newName = " + newName);
                }
                @Override
                public void fileModified(int wd, String rootPath, String fileName) {
                	log.info("wd           = " + wd);
                	if(!doFileModify){log.info("file Modify = false");return;}
//					try {
//						Thread.sleep(2000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
					log.info("fileModified" + "          " + rootPath + "           " + fileName);
					doCopyFile(rootPath, fileName);



				}
                @Override
                public void fileDeleted(int wd, String rootPath, String fileName) {
                    log.info("fileDeleted" + "          " + rootPath + "           " + fileName);
                }
                @Override
                public void fileCreated(int wd, String rootPath, String fileName) {
                    log.info("fileCreate" + "          " + rootPath + "           " + fileName);
					if (!doFileCreate){log.info("file Createe = false");return;}
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					doCopyFile(rootPath, fileName);

                }

				private void doCopyFile(String rootPath, String fileName) {
                	java.util.Calendar cal= Calendar.getInstance();
					Date runTime =   DateBuilder.futureDate(3, DateBuilder.IntervalUnit.SECOND);
					JobDetail job = JobBuilder.newJob(CopyFileJob.class).withIdentity(sdf.format(cal.getTime()), "FileCopyJobGroup").build();


					job.getJobDataMap().put("sched"						, sched 				);

					job.getJobDataMap().put("toFTP"						, toFTP 				);
					job.getJobDataMap().put("toAMQ"						, toAMQ 				);
					job.getJobDataMap().put("toOSS"						, toOSS 				);
					job.getJobDataMap().put("doFileCreate" 				, doFileCreate 			);
					job.getJobDataMap().put("doFileModify" 				, doFileModify 			);

					job.getJobDataMap().put("ftp_ip" 					, ftp_ip 				);
					job.getJobDataMap().put("ftp_port" 					, ftp_port 				);
					job.getJobDataMap().put("ftp_name" 					, ftp_name 				);
					job.getJobDataMap().put("ftp_pass" 					, ftp_pass 				);
					job.getJobDataMap().put("device_name" 				, device_name 			);
					job.getJobDataMap().put("fileRegularExpression" 	, fileRegularExpression );

					job.getJobDataMap().put("amqUrl" 	, amqUrl );
					job.getJobDataMap().put("amqTopic" 	, amqTopic );

					job.getJobDataMap().put("cameraImageUrl" 	, cameraImageUrl );
					job.getJobDataMap().put("cameraImageFileName" 	, cameraImageFileName );
					job.getJobDataMap().put("ftpToSameFile" 	, ftpToSameFile );

					job.getJobDataMap().put("rootPath" 	, rootPath );
					job.getJobDataMap().put("fileName" 	, fileName );
					job.getJobDataMap().put("delAfterFtp" 	, delAfterFtp );

					Trigger trigger = TriggerBuilder.newTrigger().withIdentity(sdf.format(cal.getTime()), "FileCopyGroup").startAt(runTime).build();
					try {
						finalScheduler.scheduleJob(job, trigger);
					} catch (SchedulerException e) {
						e.printStackTrace();
					}

					log.info("------- Started Scheduler -----------------");
					try {
						for(String group: finalScheduler.getJobGroupNames()) {
							for(JobKey jobKey : finalScheduler.getJobKeys(GroupMatcher.<JobKey>groupEquals(group))) {
								System.out.println("Found job identified by: " + jobKey);
								List<Trigger> jobTriggers = (List<Trigger>) finalScheduler.getTriggersOfJob(JobKey.jobKey("jobName", "group1"));
								//                System.out.println(jobTriggers);
							}
						}
					} catch (SchedulerException e) {
						e.printStackTrace();
					}

				}


			});
		} catch (JNotifyException e) {
			e.printStackTrace();
		}
		while (true) {

		}

	}



}

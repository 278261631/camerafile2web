import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.TaskData;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.*;
import java.net.SocketException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * This is just a simple job that says "Hello" to the world.
 * </p>
 * 
 * @author Bill Kratzer
 */
public class CopyFileJob implements Job {

    private static Logger _log = LoggerFactory.getLogger(CopyFileJob.class);


    private static Log log = LogFactory.getLog(CopyFileJob.class);
    static SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public CopyFileJob() {
    }


    @Override
    public void execute(JobExecutionContext context)
        throws JobExecutionException {
        JobDataMap data = context.getJobDetail().getJobDataMap();
            TaskData task= (TaskData) data.get("task");

        Scheduler sched                     = (Scheduler) data.get("sched"		);

        boolean toFTP                     = data.getBooleanValue("toFTP"		);
        boolean toAMQ                     = data.getBooleanValue("toAMQ"		);
        boolean toOSS                     = data.getBooleanValue("toOSS"		);
        boolean doFileCreate              = data.getBooleanValue("doFileCreate" );
        boolean doFileModify              = data.getBooleanValue("doFileModify" );
        String  ftp_ip                    = data.getString("ftp_ip" );
        String  ftp_name                  = data.getString("ftp_name" 	);
        String  ftp_pass                  = data.getString("ftp_pass" 	);
        String  device_name               = data.getString("device_name" );
        String  fileRegularExpression     = data.getString("fileRegularExpression" );
        int     ftp_port                  = data.getInt("ftp_port" 	);

        String  rootPath               = data.getString("rootPath" );
        String  fileName               = data.getString("fileName" );

        String  amqUrl               = data.getString("amqUrl" );
        boolean ftpToSameFile              = data.getBooleanValue("ftpToSameFile" );
        String  cameraImageFileName               = data.getString("cameraImageFileName" );
        String  amqTopic               = data.getString("amqTopic" );

        String  cameraImageUrl           = data.getString("cameraImageUrl" );
        boolean delAfterFtp                     = data.getBooleanValue("delAfterFtp"		);

        Pattern fileNameParttern = Pattern.compile(fileRegularExpression);
        Matcher fileNameMatcher = fileNameParttern.matcher(fileName);
        if (!fileNameMatcher.find()){
            log.info(fileName + "     !=          " + fileRegularExpression);
            return;
        }
        Path sourcePath = Paths.get(rootPath, fileName);
        String activePath = String.valueOf(sourcePath);
        waitForWirtenCompleted(new File(activePath));
        if(toFTP){
            try {
//                File file = new File(activePath);
//                log.info("exists = " + file.exists());
//                log.info("length  =" + file.length());
                if(!ftpToSameFile){
                    cameraImageFileName = new File(activePath).getName();
                }
                sendFileToFtp(activePath,ftp_ip,ftp_port,ftp_name,ftp_pass,cameraImageFileName,device_name, delAfterFtp, sched);
            } catch (IOException e) {
                e.printStackTrace();
                log.info("FTP Error "+activePath );
                log.info("FTP Error "+ftp_ip );
                log.info("FTP Error "+ftp_port);
                log.info("FTP Error "+device_name );
                log.info("FTP Error "+ftp_pass );
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }

        if(toAMQ){
            sendAMQWebCamera(rootPath,fileName,amqUrl,amqTopic, device_name, cameraImageUrl, cameraImageFileName);
        }

    }


    public void sendAMQWebCamera(String rootPath,String activeFileName ,String amqURL, String amqTopic , String deviceName, String cameraImageUrl, String cameraImageFileName){
        String jsonMessage="";
        Path sourcePath = Paths.get(rootPath, activeFileName);
        String activePath = String.valueOf(sourcePath);
        File activeFile = new File(activePath);
        if(activeFile.isFile() && activeFile.exists() ){
            log.info("file =   "+ activeFile.getAbsolutePath());
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//							Files.copy(sourcePath , Paths.get(cameraWebFileFullPath) , REPLACE_EXISTING);

            DeviceStatus deviceStatus = new DeviceStatus();
            deviceStatus.setMsgType("WebCamera");
            deviceStatus.setDeviceName(deviceName);
            deviceStatus.setCameraImage(cameraImageUrl+"/"+deviceName+"/"+cameraImageFileName);
            JSONObject jsonObject=JSONObject.fromObject(deviceStatus);
            jsonMessage=jsonObject.toString();
        }

        ConnectionFactory connectionFactory;
        Connection connection = null;
        Session session;
        Destination destination;
        MessageProducer producer;
        TextMessage message = null;
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
                , ActiveMQConnection.DEFAULT_PASSWORD, amqURL);
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            destination = session.createTopic(amqTopic);
            producer = session.createProducer(destination);
            //todo use json text message
            message=session.createTextMessage(jsonMessage);
            producer.send(message);
            session.commit();
        } catch (JMSException e) {
            log.error(e);
            e.printStackTrace();
        }finally {
            try {
                if (null != connection){
                    connection.close();
                }
            } catch (Throwable ignore) {
            }
        }
    }


    public static String putOss(File file , String ossFolder ,String bucketName){

        String return_key = null;
        try {
            OSSClient client = DefaultOSSClient.getDefaultOSSClient();

            if(file != null){

                String fileName = file.getName();
                /**
                 * getCurrentTimeStamp()方法为同步方法，确保时间戳的唯一性。

                 */
//				String timeStamp = Date2Str.getCurrentTimeStamp();
//				String timeDate = Date2Str.getCurrentDate5();
//				String key = fitsSrcPath + timeDate + timeStamp +"/"+fileName;

//				client.putObject(new PutObjectRequest("open-luna", key, file));
                PutObjectResult response = client.putObject(new PutObjectRequest("open-luna", ossFolder+fileName, file));
                response.getETag();
            }

            DefaultOSSClient.shutdownOSSClient();

        } catch (ClientException e) {
            // TODO Auto-generated catch block
            log.info("OSSUpload.put1 error:" + e.toString());
            e.printStackTrace();
            return null;
        }

        return return_key;
    }

    private void sendFileToFtp(String filePath, String ftp_ip, int ftp_port, String ftp_name, String ftp_pass, String newFileName, String subFolderName, boolean delAfterFtp, Scheduler sched) throws IOException, SchedulerException {
        if ("".equals(ftp_ip)){return;}
        System.out.println(filePath +" -->  "+ftp_ip+":"+ftp_port+":"+ftp_name+":"+subFolderName);
        InputStream in = null ;
        try {
            FTPClient ftp=new FTPClient();
            //todo FTP 连接失败也许会造成程序卡死
            ftp.connect(ftp_ip, ftp_port);
            ftp.login(ftp_name, ftp_pass);
            String ftpSbuPath=subFolderName;
            ftp.enterLocalPassiveMode();
            ftp.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);
            ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
            String ftpFileName =filePath;
            String destFileName ="";
            String ftpDestFileName=newFileName;
            in = new BufferedInputStream(new FileInputStream(ftpFileName));

            boolean isSubDirectoryExsit = false;
            FTPFile[] dirs = ftp.listDirectories();
            if (dirs != null && dirs.length > 0) {
                for (int i = 0; i < dirs.length; i++) {
                    if (dirs[i].getName().equals(ftpSbuPath)) {
                        isSubDirectoryExsit = true;
                    }
                    break;
                }
            }
            ftp.enterLocalPassiveMode();

            String LOCAL_CHARSET = "UTF-8";
            // FTP协议里面，规定文件名编码为iso-8859-1
            String SERVER_CHARSET = "ISO-8859-1";
            ftp.setControlEncoding(LOCAL_CHARSET);
            if (!isSubDirectoryExsit && !ftpSbuPath.equals("")) {
                ftp.makeDirectory(new String(ftpSbuPath.getBytes(LOCAL_CHARSET),SERVER_CHARSET));
                destFileName = ftpSbuPath + "/" + ftpDestFileName;
            }
            if (isSubDirectoryExsit && !ftpSbuPath.equals("")) {
                destFileName = ftpSbuPath + "/" + ftpDestFileName;
            }

            if (FTPReply.isPositiveCompletion(ftp.sendCommand("OPTS UTF8", "ON"))) {// 开启服务器对UTF-8的支持，如果服务器支持就用UTF-8编码，否则就使用本地编码（GBK）.
                LOCAL_CHARSET = "UTF-8";
            }
            destFileName = new String(destFileName.getBytes(LOCAL_CHARSET),SERVER_CHARSET);
//            if (!ftp.storeFile(new String(destFileName.getBytes("UTF-8"),"iso-8859-1"), in)) {
            if (!ftp.storeFile(destFileName, in)) {
                throw new IOException("Can't upload file '" + ftpFileName + "' to FTP server. Check FTP permissions and path." +ftp_ip+":"+ftp_port+":"+ftp_name + ":" + destFileName);
            }
            Thread.sleep(3000);

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            in.close();
//			try {
//				in.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
        }
        if(delAfterFtp){
            doDelFile(filePath, sched);
        }
    }


    private void waitForWirtenCompleted(File file) {
        if (!file.exists()){
            return;
        }
        long old_length;
        do {
            old_length = file.length();
            try {
                Thread.sleep(600);
                log.info("wait file ............... ");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (old_length != file.length());
        log.info("wait file ...................................OK    "+old_length);
    }


    private void doDelFile(String filePath, Scheduler sched) throws SchedulerException {
        java.util.Calendar cal= Calendar.getInstance();
        Date runTime =   DateBuilder.futureDate(10, DateBuilder.IntervalUnit.SECOND);
        JobDetail deljob = JobBuilder.newJob(DelFileJob.class).withIdentity(sdf.format(cal.getTime()), "FileDelJobGroup").build();
        deljob.getJobDataMap().put("fileName" 	, filePath );

        Trigger trigger = TriggerBuilder.newTrigger().withIdentity(sdf.format(cal.getTime()), "FileDelGroup").startAt(runTime).build();
        sched.scheduleJob(deljob, trigger);

    }


}
